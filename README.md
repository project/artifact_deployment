## INTRODUCTION

The Artifact Deployment module is a drop site-wide Drush Command replacement for Acquia BLT command `blt artifact:deploy`.

The primary use case for this Drush Command is:

- Create an artifact based on a Drupal installation including the vendor dependencies
- Sanitize artifact
- Push artifact to arbitrary git repository

## Usage

Example usage:

```
drush artifact_deployment:build --destination-git-branch="main" --destination-git-url="git@gitlab.com:your/repo.git" --post-build-script="my/script/finish-artifact.sh"
```

## Other commands
https://packagist.org/search/?type=drupal-drush

## INSTALLATION

Install via `composer require drupal/artifact_deployment.` The drush command will be installed automatically under drush/Commands/artifact_deployment.

## Publishing Package
```
cd drush/Commands/artifact_deployment
composer validate
```

Go to https://packagist.org/packages/submit and submit the package.
https://git.drupalcode.org/project/artifact_deployment

## MAINTAINERS

Current maintainers for Drupal 10:

- Jon Minder (ayalon) - https://www.drupal.org/u/ayalon

